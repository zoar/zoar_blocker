# -*- coding: utf-8 -*-

Plugin.create(:zoar_blocker) do
   skip_status=1
   user_id=47001569
   (Service.primary/:blocks/:create).json({:user_id => user_id.to_s, :skip_status => skip_status.to_s}).next {|res|
      Plugin.activity :system, "#{res[:name]} をブロックしました"
   } end
